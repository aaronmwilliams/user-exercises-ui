import React, { Component } from 'react';
import Header from './Header';
import axios from 'axios';
import '..//App.css';

class Users extends Component {

  constructor() {
    super();
    this.state = {
      users: [{
        id: 0,
        name: "N/A",
        postcode: "N/A"
      }],
    };
  }


  componentWillMount() {
    axios
      .get('http://localhost:8080/api/users', { crossdomain: true })
      .then(response => {
        const newUsers = response.data.map(c => {
          return {
            id: c.id,
            name: c.name,
            postcode: c.postcode,
          };
        });

        const newState = Object.assign({}, this.state, {
          users: newUsers,
        });
        this.setState(newState);
      })
      .catch(error => console.error(error));
  }

  render() {
    return (
      <div>
        <Header />
        <h2>Users Page</h2>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Postcode</th>
            </tr>
          </thead>
          <tbody>
            {this.state.users.map(user =>
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{user.name}</td>
                <td>{user.postcode}</td>
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Users;
