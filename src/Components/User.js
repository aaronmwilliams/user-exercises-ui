import React, { Component } from 'react';
import Header from './Header';
import axios from 'axios';
import '..//App.css';

class User extends Component {
  constructor() {
    super();
    this.state = {
      id: 0,
      name: "N/A",
      postcode: "N/A"
    };
    this.deleteUser = this.deleteUser.bind(this);
  }

  componentWillMount() {
    const { match: { params } } = this.props;
    axios
      .get('http://localhost:8080/api/users/'
        + params.userid, { crossdomain: true })
      .then(response => {
        this.setState({
          id: response.data.id,
          name: response.data.name,
          postcode: response.data.postcode,
        });
      })
      .catch(error => console.error(error));
  }

  deleteUser() {
    axios
      .delete('http://localhost:8080/api/users/'
        + this.state.id, { crossdomain: true });
    window.location.reload();
  }

  render() {
    return (
      <div>
        <Header />
        <h2>
          User Page for {this.state.name}
        </h2>
        <table>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Postcode</th>
            </tr>
          </thead>
          <tbody>
            <tr key={this.state.id}>
              <td>{this.state.id}</td>
              <td>{this.state.name}</td>
              <td>{this.state.postcode}</td>
            </tr>
          </tbody>
        </table>
        <button onClick={this.deleteUser}>Delete User</button>
      </div>
    );
  }
}

export default User;
