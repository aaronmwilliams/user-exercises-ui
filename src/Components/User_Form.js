import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

class UserForm extends Component {

	constructor() {
		super();
		this.state = {
			userid: '',
		};
	}

	updateState(userInput) {
		this.setState({
			userid: userInput,
		});
	}

	render() {
		return (
			<div>
				<label>Enter User ID to find user:
					<input type="text"
						className="userid"
						name="userid"
						onChange={(evt) => {
							this.updateState(evt.target.value);
						}}
					/>
					<Link to={`/user/${this.state.userid}`}
						id="userlink"
						className={this.state.userid ? 'enabled-link' : 'disabled-link'}>
						Find User
					</Link>
				</label>
			</div>
		);
	}
}

export default UserForm;
