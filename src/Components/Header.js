import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

class Header extends Component {
	render() {
		return (
			<div>
				<h1>user-exercises-ui</h1>
				<Link
					className="nav_links"
					data-qa="home_link"
					to="/">Homepage
				</Link>

				<Link
					className="nav_links"
					data-qa="users_link"
					to="/users">Users
				</Link>
			</div>
		);
	}
}

export default Header;
