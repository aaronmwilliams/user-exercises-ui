import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Route } from 'react-router-dom';
import App from './App';
import Users from './Components/Users';
import User from './Components/User';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
	<BrowserRouter>
		<React.Fragment>
			<Route exact path="/" component={App} />
			<Route path="/users" component={Users} />
			<Route path="/user/:userid" component={User} />
		</React.Fragment>
	</BrowserRouter>,
	document.getElementById('root')
);

registerServiceWorker();
