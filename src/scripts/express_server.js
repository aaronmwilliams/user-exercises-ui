import express from 'express';
import users from '../../fixtures/mocked_users.json';
import user from '../../fixtures/mocked_user.json';

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.get('/api/users/:userId', (req, res) => {
  res.json(user);
});

app.get('/api/users', (req, res) => {
  res.json(users);
});

app.listen(8080);
