import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../Components/Header';

describe('when header component renders', () => {

  const wrapper = shallow(<Header />);

  it('should display "user-exercises-ui" text', () => {
    expect(wrapper.find('h1').text()).toBe('user-exercises-ui');
  });

  it('should contain navigation links for Homepage & Users', () => {
    const homePageLink = wrapper.find('[data-qa="home_link"]');
    expect(homePageLink.props().to).toBe('/');
    expect(homePageLink.contains('Homepage')).toBe(true);

    const usersLink = wrapper.find('[data-qa="users_link"]');
    expect(usersLink.props().to).toBe('/users');
    expect(usersLink.contains('Users')).toBe(true);
  });

  it('should match previous snapshot', () => {
    const tree = renderer.create(
      <Router>
        <Header />
      </Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

});
