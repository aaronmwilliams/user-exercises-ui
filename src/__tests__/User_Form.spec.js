import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';
import UserForm from '../Components/User_Form';

const wrapper = shallow(<UserForm />);

describe('when user form component renders', () => {
  it('have the link disabled', () => {
    expect(wrapper.find('#userlink').hasClass('disabled-link')).toBe(true);
    expect(wrapper.find('label').text()).toBe('Enter User ID to find user:<Link />');
  });

  it('have the search form', () => {
    expect(wrapper.find('label').text()).toBe('Enter User ID to find user:<Link />');
  });



  it('should match previous snapshot', () => {
    const tree = renderer.create(
      <Router>
        <UserForm />
      </Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('when user form received input', () => {
  beforeEach(() => {
    wrapper.find('input').simulate('change', {
      target: {
        value: 2,
      },
    });
  });

  it('should update state', () => {
    expect(wrapper.state('userid')).toEqual(2);
  });

  it('should enable the link', () => {
    const findUserLink = wrapper.find('#userlink');
    expect(findUserLink.props().to).toBe('/user/2');
    expect(findUserLink.hasClass('enabled-link')).toBe(true);
  });
});
