import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../Components/Header';

describe('when main app page loads', () => {
  const wrapper = shallow(<App />);

  it('should contain header component', () => {
    expect(wrapper.contains(<Header />)).toBe(true);
  });

  it('should display "Homepage" text', () => {
    expect(wrapper.find('h2').text()).toBe('Homepage');
  });

  it('should match previous snapshot', () => {
    const tree = renderer.create(
      <Router>
        <App />
      </Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

});
