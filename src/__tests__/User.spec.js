import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import User from '../Components/User';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from '../Components/Header';

describe('when user component renders', () => {

  const match = { params: { userid: '1' } }
  const spyOn = jest.spyOn(User.prototype, "deleteUser");
  const wrapper = shallow(<User match={match} />);

  it('should default state to N/A', () => {
    expect(wrapper.state('id')).toEqual(0)
    expect(wrapper.state('name')).toEqual('N/A')
    expect(wrapper.state('postcode')).toEqual('N/A')
  })

  it('should contain header component', () => {
    expect(wrapper.contains(<Header />)).toBe(true);
  });

  it('should contain heading with username from props', () => {
    expect(wrapper.find('h2').text()).toBe('User Page for N/A');
  });

  it('should call delete user function on button click', () => {
    wrapper.find("button").simulate('click');
    expect(spyOn).toHaveBeenCalled();
  })

  it('should contain table headers', () => {
    expect(wrapper.contains('ID')).toBe(true);
    expect(wrapper.contains('Name')).toBe(true);
    expect(wrapper.contains('Postcode')).toBe(true);
  });

  it('should match previous snapshot', () => {
    const tree = renderer.create(
      <Router>
        <User match={match} />
      </Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

});
