import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { BrowserRouter as Router } from 'react-router-dom';
import Users from '../Components/Users';
import Header from '../Components/Header';

describe('when users component renders', () => {
  const wrapper = shallow(<Users />);

  it('should contain header component', () => {
    expect(wrapper.contains(<Header />)).toBe(true);
  });

  it('should default state to N/A', () => {
    expect(wrapper.state('users')).toEqual(
      [{
        id: 0,
        name: "N/A",
        postcode: "N/A"
      }])
  });

  it('should contain "Users Page" text', () => {
    expect(wrapper.find('h2').text()).toBe('Users Page');
  });

  it('should contain table headers', () => {
    expect(wrapper.contains('ID')).toBe(true);
    expect(wrapper.contains('Name')).toBe(true);
    expect(wrapper.contains('Postcode')).toBe(true);
  });

  it('should match previous snapshot', () => {
    const tree = renderer.create(
      <Router>
        <Users />
      </Router>
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });

});
