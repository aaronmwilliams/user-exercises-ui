import React, { Component } from 'react';
import Header from './Components/Header';
import UserForm from './Components/User_Form';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <h2>Homepage</h2>
        <UserForm />
      </div>
    );
  }
}

export default App;
