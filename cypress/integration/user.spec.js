describe('when visiting the user page for a given user', () => {

  beforeEach(() => {
    cy.server()
    cy.route({
      method: 'GET',
      url: '**/users/*',
      response: {
        id: 1,
        name: "Aaron",
        postcode: "AA129LG"
      }
    })
  })

  it('should display "User Page for [USERNAME]"', () => {
    cy.visit('/user/1')
    cy.get('h2')
      .should('have.text', 'User Page for Aaron')
  });

});
