describe('when visiting the homepage', () => {
  it('should display welcome', () => {
    cy.visit('/');
    cy.get("h1")
      .should("have.text", "user-exercises-ui");
  });
});