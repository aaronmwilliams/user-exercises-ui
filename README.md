# user-exercises-ui

### Purpose of project
A very simple React app to help aid other example test frameworks in this repository.

### Installing and Running

#### Run via Node
Checkout the project and run `yarn install`

To start the app run `yarn start`

You can run an express server to stub the API when running. To do this run command `npm run mock:start`.

#### Run via Docker
You can run the app via Docker.

*Docker Compose* `docker-compose build` then `docker-compose up`

or...

*Docker Image* `docker build -f Dockerfile -t user-exercises-ui .` then `docker run -d -p 3000:3000 user-exercises-ui`

To stop the container running run command `docker ps` get the container ID then run `docker stop [Container ID]`

By default the app will run on http://localhost:3000/

### Testing
- Unit tests
- Snapshot tests
- Cypress Functional End to End Tests (with mocked API)

#### Run Locally
If you have Node & Cypress installed you can run the app and tests locally.

To run the unit and snapshot tests in terminal call command `yarn test`.

You can check the unit test coverage by running `npm run test:coverage`.

To run the Cypress tests run `cypress run` with the app running locally.


#### Run Tests via Docker
If you have Docker installed on your machine you can run the app and tests using Docker compose: `docker-compose -f docker-compose-cypress.yml build` then `docker-compose -f docker-compose-cypress.yml up`. The app and tests will start automatically.

If you need to update the snapshot images run `yarn test -- -u`

### Supporting Repositories
This app requires the API `user-exercises-rest` to be running please visit https://bitbucket.org/aaronmwilliams/user-exercises-rest for details.